# VAIS Python SDK
## Getting started
### Installation
#### Dependencies
```
[sudo] apt-get install python-pyaudio vlc libvlc-dev libspeex-dev libspeexdsp-dev g++ gcc
[sudo] pip install speex
```

#### Install `vaiskit`
```
[sudo] pip install vaiskit==0.1.1b2
```

### Configuration
To record audio correctly, you must configure the default sound card as follows,

Creates file /etc/modprobe.d/alsa-base.conf with the following content,
```
options snd-card-usb-caiaq index=0
```

Tips: run `arecord --list-devices` for the list of audio devices.

# Known Issues
On some ARM devices, you might encounter the below error,
   ```
   FATAL: cannot locate cpu MHz in /proc/cpuinfo
   ```
see [this post](https://stackoverflow.com/questions/35722745/pyaudio-fatal-error-on-arm-environment) for a solution.

