import os
from setuptools import setup

if __name__ == "__main__":
    setup(
        name="vaiskit",
        author='VAIS',
        packages=['vaiskit'],
        package_data={'': ['./vaiskit/nlp', './vaiskit/speech']},
        author_email='support@vais.vn',
        url='https://vais.vn',
        include_package_data=True,
        install_requires=["grpcio==1.4.0", "pyaudio==0.2.11", "webrtcvad==2.0.10", "pyyaml", "future", "python-vlc"],
        version="0.1.1.b3",
        python_requires='>=2.6, !=3.0.*, !=3.1.*, !=3.2.*, <4',
    )
