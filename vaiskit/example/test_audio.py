#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 Truong Do <truongdq54@gmail.com>
#
# from vaiskit.capture import Capture
import yaml
import logging
from vaiskit.capture import DeviceInfo, Capture
logging.basicConfig(level=logging.DEBUG)

def state_callback(*args):
    print(*args)

config = yaml.load(open("./config.yaml"))
a = Capture(config, "/tmp/audio")
a.setup(state_callback)
audio_stream = a.silence_listener()
for data in audio_stream:
    print(len(data))
input("Enter to finish")
# device_info = DeviceInfo()
# print(a.get_device_list())
