#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 VAIS <support@vais.vn>
#
from __future__ import print_function
import os
import time
import logging
from vaiskit.vais import VaisService
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

def on_asr_result(text):
    # Return ASR text in real-time
    logger.debug("ASR output: %s", text)

def on_intent_result(result):
    # Final intent recognition result here
    logger.info("Final result")
    logger.info(result)

def on_slot_filling(text):
    logger.info(text)


if __name__ == "__main__":
    path = os.path.realpath(__file__).rstrip(os.path.basename(__file__))
    resources_path = os.path.join(path, 'resources', '')
    with VaisService("demo", "./config.yaml", resources_path=resources_path,
                     silence=False) as vais_service:
        vais_service.asr_callback = on_asr_result
        vais_service.intent_callback = on_intent_result
        vais_service.slot_filling_callback = on_slot_filling
        vais_service.intent_recognize("e2039cd1-4a88-4283-9910-7b808d8fca53")  # This call will be finished once the on_intent_result is returned
    input("")
