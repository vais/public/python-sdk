import vaiskit.tunein as tunein
from vaiskit.basehandler import RequestType, PlayerActivity
import logging
import threading
import hashlib

logger = logging.getLogger(__name__)
class Player(object):

    config = None
    platform = None
    pHandler = None
    tunein_parser = None

    navigation_token = None
    playlist_last_item = None
    progressReportRequired = []

    def __init__(self, config, platform, pHandler): # pylint: disable=redefined-outer-name
        self.config = config
        self.platform = platform
        self.pHandler = pHandler # pylint: disable=invalid-name
        self.tunein_parser = tunein.TuneIn(5000)

    # def play_playlist(self, payload):
        # self.navigation_token = payload['navigationToken']
        # self.playlist_last_item = payload['audioItem']['streams'][-1]['streamId']

        # for stream in payload['audioItem']['streams']: # pylint: disable=redefined-outer-name

            # streamId = stream['streamId']
            # if stream['progressReportRequired']:
                # self.progressReportRequired.append(streamId)

            # url = stream['streamUrl']
            # if stream['streamUrl'].startswith("cid:"):
                # url = "file://" + tmp_path + hashlib.md5(stream['streamUrl'].replace("cid:", "", 1).encode()).hexdigest() + ".mp3"

            # if (url.find('radiotime.com') != -1):
                # url = self.tunein_playlist(url)

            # self.pHandler.queued_play(url, stream['offsetInMilliseconds'], audio_type='media', stream_id=streamId)

    def play_speech(self, mrl):
        self.stop()
        self.pHandler.blocking_play(mrl)

    def stop(self):
        self.pHandler.stop()

    def is_playing(self):
        return self.pHandler.is_playing()

    def get_volume(self):
        return self.pHandler.volume

    def set_volume(self, volume):
        self.pHandler.set_volume(volume)

    def playback_callback(self, requestType, playerActivity, streamId):
        if (requestType == RequestType.STARTED) and (playerActivity == PlayerActivity.PLAYING):
            self.platform.indicate_playback()
        elif (requestType in [RequestType.INTERRUPTED, RequestType.FINISHED, RequestType.ERROR]) and (playerActivity == PlayerActivity.IDLE):
            self.platform.indicate_playback(False)

        # if streamId:
            # if streamId in self.progressReportRequired:
                # self.progressReportRequired.remove(streamId)
                # gThread = threading.Thread(target=alexa_playback_progress_report_request, args=(requestType, playerActivity, streamId))
                # gThread.start()

            # if (requestType == RequestType.FINISHED) and (playerActivity == PlayerActivity.IDLE) and (self.playlist_last_item == streamId):
                # gThread = threading.Thread(target=alexa_getnextitem, args=(self.navigation_token,))
                # self.navigation_token = None
                # gThread.start()

    # def tunein_playlist(self, url):
        # logger.debug("TUNE IN URL = %s", url)

        # req = requests.get(url)
        # lines = req.content.decode().split('\n')

        # nurl = self.tunein_parser.parse_stream_url(lines[0])
        # if nurl:
            # return nurl[0]

        # return ""
