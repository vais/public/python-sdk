from __future__ import print_function
import uuid
import grpc
import json
import shutil
import yaml
import vaiskit.speech.v1.cloud_speech_pb2_grpc as cloud_speech_pb2_grpc
import vaiskit.speech.v1.cloud_speech_pb2 as cloud_speech_pb2
import vaiskit.nlp.v1.cloud_nlp_pb2_grpc as cloud_nlp_pb2_grpc
import threading
import importlib
import vaiskit.capture as capture
from vaiskit.player import Player
from vaiskit.baseplatform import BasePlatform
from vaiskit.vlchandler import VlcHandler

try:
    import pyaudio
except Exception as e:
    print("You don't have pyaudio installed")
    exit(1)

CHUNK = 640
encoder = None
try:
    from speex import WBEncoder
    encoder = WBEncoder()
    encoder.quality = 10
    packet_size = encoder.frame_size
    CHUNK = packet_size
except Exception as e:
    print("******** WARNING ******************************************************")
    print("* You don't have pyspeex https://github.com/NuanceDev/pyspeex installed. You might suffer from low speed performance! *")
    print("")
    print("***********************************************************************")

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
RECORD_SECONDS = 15

class VaisService():
    def __init__(self, api_key, config_file, resources_path=None, silence=False):
        self.api_key = api_key
        self.url = "service.grpc.vais.vn:50051"
        self.config = yaml.load(open(config_file))
        self.session = str(uuid.uuid4())
        self.channel = grpc.insecure_channel(self.url)
        self.channel.subscribe(callback=self.connectivity_callback, try_to_connect=True)
        self.stop = False
        self.stub = cloud_speech_pb2_grpc.SpeechStub(self.channel)
        self.stub_nlu = cloud_nlp_pb2_grpc.NLPStub(self.channel)
        self.asr_callback = None
        self.intent_callback = None
        self.slot_filling_callback = None
        self.silence = silence
        self.platform = BasePlatform(self.config, "raspberrypi")
        self.capture = capture.Capture(self.config, "/tmp/audio")
        self.resources_path = resources_path

        self.pHandler = VlcHandler(self.config, self.playback_callback)
        self.player = Player(self.config, self.platform, self.pHandler)

    def renew_session(self):
        self.session = str(uuid.uuid4())

    def playback_callback(self, requestType, playerActivity, streamId):
        return self.player.playback_callback(requestType, playerActivity, streamId)

    def state_callback(*args):
        pass

    def __enter__(self):
        self.capture.setup(self.state_callback)
        self.pHandler.setup()
        # if not self.silence:
            # self.player.play_speech(self.resources_path + "hello_lumi.mp3")
        return self

    def __exit__(self, *args):
        self.capture.cleanup()
        self.platform.cleanup()
        self.pHandler.cleanup()
        shutil.rmtree("/tmp/audio")

    def connectivity_callback(self, c):
        pass

    def get_speech(self):
        audio_stream = self.capture.silence_listener()
        for data in audio_stream:
            if encoder:
                data = encoder.encode(data)
            yield data

    def generate_messages(self, app_id):
        sc = cloud_speech_pb2.SpeechContext(app_id=app_id)
        audio_encode = cloud_speech_pb2.RecognitionConfig.SPEEX_WITH_HEADER_BYTE
        if encoder is None:
            audio_encode = cloud_speech_pb2.RecognitionConfig.LINEAR16

        config = cloud_speech_pb2.RecognitionConfig(speech_contexts=[sc], encoding=audio_encode)
        streaming_config = cloud_speech_pb2.StreamingRecognitionConfig(config=config, single_utterance=True, interim_results=True, session=self.session)
        request = cloud_speech_pb2.StreamingRecognizeRequest(streaming_config=streaming_config)
        yield request
        for audio in self.get_speech():
            request = cloud_speech_pb2.StreamingRecognizeRequest(audio_content=audio)
            yield request

    def _start_intent_recognize(self, app_id):
        metadata = [(b'api-key', self.api_key)]
        self.player.play_speech(self.resources_path + "beep.wav")
        self.capture._interrupt = False
        try:
            responses = self.stub.StreamingIntentRecognize(self.generate_messages(app_id=app_id), metadata=metadata)
            for response in responses:
                try:
                    if response.is_final:
                        self.capture._interrupt = True
                        result = response.intent_result
                        if result.need_more_info:
                            if self.slot_filling_callback:
                                if result.audio_response and not self.silence:
                                    self.player.play_speech(result.audio_response)
                                self.slot_filling_callback(result.text_response)
                            self._start_intent_recognize(app_id)
                        else:
                            parameters = {}
                            try:
                                parameters = json.loads(result.parameters)
                            except Exception as e:
                                print(e)
                            self.intent_callback({'intent': result.intent, 'parameters': parameters, 'confidence': result.confidence})
                            if result.audio_response and not self.silence:
                                self.player.play_speech(result.audio_response)
                    else:
                        if self.asr_callback:
                            self.asr_callback(response.asr_response.results[0].alternatives[0].transcript)
                except Exception as e:
                    print(e)
                    pass

        except grpc._channel._Rendezvous as e:
            print(e)

    def intent_recognize(self, app_id):
        self._start_intent_recognize(app_id)
        return None

if __name__ == "__main__":
    a = VaisService("demo")
    for is_final, result in a.intent_recognize("e2039cd1-4a88-4283-9910-7b808d8fca53"):
        print(is_final, result)
