# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: nlp/v1/cloud_nlp.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='nlp/v1/cloud_nlp.proto',
  package='vais.cloud.nlp.v1',
  syntax='proto3',
  serialized_pb=_b('\n\x16nlp/v1/cloud_nlp.proto\x12\x11vais.cloud.nlp.v1\"G\n\x16IntentRecognizeRequest\x12\x0c\n\x04text\x18\x01 \x01(\t\x12\x0e\n\x06\x61pp_id\x18\x02 \x01(\t\x12\x0f\n\x07session\x18\x03 \x01(\t\"$\n\x12GetSentenceRequest\x12\x0e\n\x06\x61pp_id\x18\x01 \x01(\t\"&\n\x13GetSentenceResponse\x12\x0f\n\x07phrases\x18\x01 \x03(\t\"\xaa\x01\n\x15IntentRecognizeResult\x12\x12\n\ntranscript\x18\x01 \x01(\t\x12\x0e\n\x06intent\x18\x02 \x01(\t\x12\x12\n\nparameters\x18\x03 \x01(\t\x12\x15\n\rtext_response\x18\x04 \x01(\t\x12\x16\n\x0e\x61udio_response\x18\x05 \x01(\t\x12\x16\n\x0eneed_more_info\x18\x06 \x01(\x08\x12\x12\n\nconfidence\x18\x07 \x01(\x02\x32\xdd\x01\n\x03NLP\x12h\n\x0fIntentRecognize\x12).vais.cloud.nlp.v1.IntentRecognizeRequest\x1a(.vais.cloud.nlp.v1.IntentRecognizeResult\"\x00\x12l\n\x19GetSentenceForApplication\x12%.vais.cloud.nlp.v1.GetSentenceRequest\x1a&.vais.cloud.nlp.v1.GetSentenceResponse\"\x00\x42%\n\x14vn.vais.cloud.nlp.v1B\x08NlpProtoP\x01\xf8\x01\x01\x62\x06proto3')
)




_INTENTRECOGNIZEREQUEST = _descriptor.Descriptor(
  name='IntentRecognizeRequest',
  full_name='vais.cloud.nlp.v1.IntentRecognizeRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='text', full_name='vais.cloud.nlp.v1.IntentRecognizeRequest.text', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='app_id', full_name='vais.cloud.nlp.v1.IntentRecognizeRequest.app_id', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='session', full_name='vais.cloud.nlp.v1.IntentRecognizeRequest.session', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=45,
  serialized_end=116,
)


_GETSENTENCEREQUEST = _descriptor.Descriptor(
  name='GetSentenceRequest',
  full_name='vais.cloud.nlp.v1.GetSentenceRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='app_id', full_name='vais.cloud.nlp.v1.GetSentenceRequest.app_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=118,
  serialized_end=154,
)


_GETSENTENCERESPONSE = _descriptor.Descriptor(
  name='GetSentenceResponse',
  full_name='vais.cloud.nlp.v1.GetSentenceResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='phrases', full_name='vais.cloud.nlp.v1.GetSentenceResponse.phrases', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=156,
  serialized_end=194,
)


_INTENTRECOGNIZERESULT = _descriptor.Descriptor(
  name='IntentRecognizeResult',
  full_name='vais.cloud.nlp.v1.IntentRecognizeResult',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='transcript', full_name='vais.cloud.nlp.v1.IntentRecognizeResult.transcript', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='intent', full_name='vais.cloud.nlp.v1.IntentRecognizeResult.intent', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='parameters', full_name='vais.cloud.nlp.v1.IntentRecognizeResult.parameters', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='text_response', full_name='vais.cloud.nlp.v1.IntentRecognizeResult.text_response', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='audio_response', full_name='vais.cloud.nlp.v1.IntentRecognizeResult.audio_response', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='need_more_info', full_name='vais.cloud.nlp.v1.IntentRecognizeResult.need_more_info', index=5,
      number=6, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='confidence', full_name='vais.cloud.nlp.v1.IntentRecognizeResult.confidence', index=6,
      number=7, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=197,
  serialized_end=367,
)

DESCRIPTOR.message_types_by_name['IntentRecognizeRequest'] = _INTENTRECOGNIZEREQUEST
DESCRIPTOR.message_types_by_name['GetSentenceRequest'] = _GETSENTENCEREQUEST
DESCRIPTOR.message_types_by_name['GetSentenceResponse'] = _GETSENTENCERESPONSE
DESCRIPTOR.message_types_by_name['IntentRecognizeResult'] = _INTENTRECOGNIZERESULT
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

IntentRecognizeRequest = _reflection.GeneratedProtocolMessageType('IntentRecognizeRequest', (_message.Message,), dict(
  DESCRIPTOR = _INTENTRECOGNIZEREQUEST,
  __module__ = 'nlp.v1.cloud_nlp_pb2'
  # @@protoc_insertion_point(class_scope:vais.cloud.nlp.v1.IntentRecognizeRequest)
  ))
_sym_db.RegisterMessage(IntentRecognizeRequest)

GetSentenceRequest = _reflection.GeneratedProtocolMessageType('GetSentenceRequest', (_message.Message,), dict(
  DESCRIPTOR = _GETSENTENCEREQUEST,
  __module__ = 'nlp.v1.cloud_nlp_pb2'
  # @@protoc_insertion_point(class_scope:vais.cloud.nlp.v1.GetSentenceRequest)
  ))
_sym_db.RegisterMessage(GetSentenceRequest)

GetSentenceResponse = _reflection.GeneratedProtocolMessageType('GetSentenceResponse', (_message.Message,), dict(
  DESCRIPTOR = _GETSENTENCERESPONSE,
  __module__ = 'nlp.v1.cloud_nlp_pb2'
  # @@protoc_insertion_point(class_scope:vais.cloud.nlp.v1.GetSentenceResponse)
  ))
_sym_db.RegisterMessage(GetSentenceResponse)

IntentRecognizeResult = _reflection.GeneratedProtocolMessageType('IntentRecognizeResult', (_message.Message,), dict(
  DESCRIPTOR = _INTENTRECOGNIZERESULT,
  __module__ = 'nlp.v1.cloud_nlp_pb2'
  # @@protoc_insertion_point(class_scope:vais.cloud.nlp.v1.IntentRecognizeResult)
  ))
_sym_db.RegisterMessage(IntentRecognizeResult)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), _b('\n\024vn.vais.cloud.nlp.v1B\010NlpProtoP\001\370\001\001'))
try:
  # THESE ELEMENTS WILL BE DEPRECATED.
  # Please use the generated *_pb2_grpc.py files instead.
  import grpc
  from grpc.beta import implementations as beta_implementations
  from grpc.beta import interfaces as beta_interfaces
  from grpc.framework.common import cardinality
  from grpc.framework.interfaces.face import utilities as face_utilities


  class NLPStub(object):
    # missing associated documentation comment in .proto file
    pass

    def __init__(self, channel):
      """Constructor.

      Args:
        channel: A grpc.Channel.
      """
      self.IntentRecognize = channel.unary_unary(
          '/vais.cloud.nlp.v1.NLP/IntentRecognize',
          request_serializer=IntentRecognizeRequest.SerializeToString,
          response_deserializer=IntentRecognizeResult.FromString,
          )
      self.GetSentenceForApplication = channel.unary_unary(
          '/vais.cloud.nlp.v1.NLP/GetSentenceForApplication',
          request_serializer=GetSentenceRequest.SerializeToString,
          response_deserializer=GetSentenceResponse.FromString,
          )


  class NLPServicer(object):
    # missing associated documentation comment in .proto file
    pass

    def IntentRecognize(self, request, context):
      # missing associated documentation comment in .proto file
      pass
      context.set_code(grpc.StatusCode.UNIMPLEMENTED)
      context.set_details('Method not implemented!')
      raise NotImplementedError('Method not implemented!')

    def GetSentenceForApplication(self, request, context):
      # missing associated documentation comment in .proto file
      pass
      context.set_code(grpc.StatusCode.UNIMPLEMENTED)
      context.set_details('Method not implemented!')
      raise NotImplementedError('Method not implemented!')


  def add_NLPServicer_to_server(servicer, server):
    rpc_method_handlers = {
        'IntentRecognize': grpc.unary_unary_rpc_method_handler(
            servicer.IntentRecognize,
            request_deserializer=IntentRecognizeRequest.FromString,
            response_serializer=IntentRecognizeResult.SerializeToString,
        ),
        'GetSentenceForApplication': grpc.unary_unary_rpc_method_handler(
            servicer.GetSentenceForApplication,
            request_deserializer=GetSentenceRequest.FromString,
            response_serializer=GetSentenceResponse.SerializeToString,
        ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
        'vais.cloud.nlp.v1.NLP', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


  class BetaNLPServicer(object):
    """The Beta API is deprecated for 0.15.0 and later.

    It is recommended to use the GA API (classes and functions in this
    file not marked beta) for all further purposes. This class was generated
    only to ease transition from grpcio<0.15.0 to grpcio>=0.15.0."""
    # missing associated documentation comment in .proto file
    pass
    def IntentRecognize(self, request, context):
      # missing associated documentation comment in .proto file
      pass
      context.code(beta_interfaces.StatusCode.UNIMPLEMENTED)
    def GetSentenceForApplication(self, request, context):
      # missing associated documentation comment in .proto file
      pass
      context.code(beta_interfaces.StatusCode.UNIMPLEMENTED)


  class BetaNLPStub(object):
    """The Beta API is deprecated for 0.15.0 and later.

    It is recommended to use the GA API (classes and functions in this
    file not marked beta) for all further purposes. This class was generated
    only to ease transition from grpcio<0.15.0 to grpcio>=0.15.0."""
    # missing associated documentation comment in .proto file
    pass
    def IntentRecognize(self, request, timeout, metadata=None, with_call=False, protocol_options=None):
      # missing associated documentation comment in .proto file
      pass
      raise NotImplementedError()
    IntentRecognize.future = None
    def GetSentenceForApplication(self, request, timeout, metadata=None, with_call=False, protocol_options=None):
      # missing associated documentation comment in .proto file
      pass
      raise NotImplementedError()
    GetSentenceForApplication.future = None


  def beta_create_NLP_server(servicer, pool=None, pool_size=None, default_timeout=None, maximum_timeout=None):
    """The Beta API is deprecated for 0.15.0 and later.

    It is recommended to use the GA API (classes and functions in this
    file not marked beta) for all further purposes. This function was
    generated only to ease transition from grpcio<0.15.0 to grpcio>=0.15.0"""
    request_deserializers = {
      ('vais.cloud.nlp.v1.NLP', 'GetSentenceForApplication'): GetSentenceRequest.FromString,
      ('vais.cloud.nlp.v1.NLP', 'IntentRecognize'): IntentRecognizeRequest.FromString,
    }
    response_serializers = {
      ('vais.cloud.nlp.v1.NLP', 'GetSentenceForApplication'): GetSentenceResponse.SerializeToString,
      ('vais.cloud.nlp.v1.NLP', 'IntentRecognize'): IntentRecognizeResult.SerializeToString,
    }
    method_implementations = {
      ('vais.cloud.nlp.v1.NLP', 'GetSentenceForApplication'): face_utilities.unary_unary_inline(servicer.GetSentenceForApplication),
      ('vais.cloud.nlp.v1.NLP', 'IntentRecognize'): face_utilities.unary_unary_inline(servicer.IntentRecognize),
    }
    server_options = beta_implementations.server_options(request_deserializers=request_deserializers, response_serializers=response_serializers, thread_pool=pool, thread_pool_size=pool_size, default_timeout=default_timeout, maximum_timeout=maximum_timeout)
    return beta_implementations.server(method_implementations, options=server_options)


  def beta_create_NLP_stub(channel, host=None, metadata_transformer=None, pool=None, pool_size=None):
    """The Beta API is deprecated for 0.15.0 and later.

    It is recommended to use the GA API (classes and functions in this
    file not marked beta) for all further purposes. This function was
    generated only to ease transition from grpcio<0.15.0 to grpcio>=0.15.0"""
    request_serializers = {
      ('vais.cloud.nlp.v1.NLP', 'GetSentenceForApplication'): GetSentenceRequest.SerializeToString,
      ('vais.cloud.nlp.v1.NLP', 'IntentRecognize'): IntentRecognizeRequest.SerializeToString,
    }
    response_deserializers = {
      ('vais.cloud.nlp.v1.NLP', 'GetSentenceForApplication'): GetSentenceResponse.FromString,
      ('vais.cloud.nlp.v1.NLP', 'IntentRecognize'): IntentRecognizeResult.FromString,
    }
    cardinalities = {
      'GetSentenceForApplication': cardinality.Cardinality.UNARY_UNARY,
      'IntentRecognize': cardinality.Cardinality.UNARY_UNARY,
    }
    stub_options = beta_implementations.stub_options(host=host, metadata_transformer=metadata_transformer, request_serializers=request_serializers, response_deserializers=response_deserializers, thread_pool=pool, thread_pool_size=pool_size)
    return beta_implementations.dynamic_stub(channel, 'vais.cloud.nlp.v1.NLP', cardinalities, options=stub_options)
except ImportError:
  pass
# @@protoc_insertion_point(module_scope)
