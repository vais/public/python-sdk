class BasePlatform:
    def __init__(self, config, platform_name):
        self._config = config

        self._pconfig = {}
        if config['platforms']['common']:
            self._pconfig = config['platforms']['common']

        if config['platforms'][platform_name]:
            self._pconfig.update(config['platforms'][platform_name])

    def setup(self):
        pass

    def after_setup(self, trigger_callback=None):
        pass

    def indicate_failure(self):
        pass

    def indicate_success(self):
        pass

    def indicate_recording(self, state=True):
        pass

    def indicate_playback(self, state=True):
        pass

    def indicate_processing(self, state=True):
        pass

    def force_recording(self):
        pass

    def cleanup(self):
        pass
